<?php
/*Începând cu inițierea unei sesiuni, acest script PHP continuă să verifice existența unei variabile de sesiune numită "utilizator_id".
În cazul în care variabila respectivă nu este definită, utilizatorul este redirecționat către pagina de conectare (conectare.php) 
și scriptul este terminat.*/

session_start();
if (!isset($_SESSION['utilizator_id'])) {
    header("Location: conectare.php");
    exit;
}
?>