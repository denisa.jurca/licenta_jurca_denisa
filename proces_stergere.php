<?php
/*
Următoarea explicație oferă o imagine de ansamblu concisă a funcționalității scriptului PHP, care implică primirea datelor prin metoda 
POST și executarea unei operații de ștergere in baza de date.

Verificarea metodei de solicitare este utilizată pentru a verifica dacă cererea HTTP corespunde tipului POST, asigurând astfel că 
transmiterea datelor are loc prin intermediul formularului web.

Procesul de extragere a ID-ului tranzacției implică examinarea datelor transmise pentru a determina prezența unui parametru 
"idTranzactie" și, ulterior, extragerea identificatorului unic asociat acestuia.

Stabilirea unei conexiuni la baza de date presupune utilizarea datelor furnizate în fișierul database.php.

Procesul de pregătire și execuție a interogării de ștergere SQL implică crearea unei interogări SQL special concepută pentru a elimina 
o tranzacție din tabelul "tranzacții" pe baza ID-ului furnizat. Ulterior, interogarea este executată, iar rezultatul operației este 
reflectat într-un răspuns JSON, indicând dacă ștergerea a avut succes sau nu.

Pentru a elibera resurse, instrucțiunea pregătită și conexiunea la baza de date sunt închise după executarea interogării.

La finalizarea operațiunii de ștergere, se transmite un răspuns în format JSON pentru a indica dacă operația a avut succes sau nu.*/

include_once('database.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["idTranzactie"])) {
        $idTranzactie = $_POST["idTranzactie"];

        $mysqli = require __DIR__ . "/database.php";
        $sql = "DELETE FROM tranzactii WHERE id = ?";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param('i', $idTranzactie);

        $response = ["success" => false];

        if ($stmt->execute()) {
            $response["success"] = true;
        }

        $stmt->close();
        $mysqli->close();

        echo json_encode($response);
    }
}
?>

