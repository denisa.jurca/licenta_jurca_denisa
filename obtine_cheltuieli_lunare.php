<?php
/*Următorul cod PHP este conceput pentru a gestiona o solicitare GET aferentă unei anumite luni și pentru a furniza cheltuielile totale 
corespunzătoare, clasificate în funcție de tipul de cheltuieli. Iată o prezentare concisă a funcționalității codului:

În procesul de examinare a parametrului GET, se efectuează o evaluare pentru a determina dacă parametrul lună a fost definit și dacă 
este o valoare numerică. Dacă această condiție este îndeplinită, valoarea este ulterior extrasă și transformată într-un tip de date 
întreg.

Generarea unei interogări SQL este efectuată pentru a calcula cheltuielile totale pentru o categorie și o lună desemnate. Pentru a 
organiza ieșirea, clauza GROUP BY este folosită pentru a grupa rezultatele în funcție de categorie.

Executarea interogării conduce la stocarea rezultatelor obţinute într-un tablou asociativ.

În timpul procesului de extragere a datelor, este necesar să se examineze meticulos fiecare rând al rezultatului pentru a extrage 
categoriile relevante și sumele corespunzătoare, care sunt ulterior stocate în matrice distincte.

La preluarea datelor necesare, este nevoie să se închidă conexiunea la baza de date pentru a elibera resursele alocate.

Răspunsul primit în format JSON include suma cumulativă a cheltuielilor lunare, o listă cuprinzătoare de categorii și valorile lor 
monetare corespunzătoare. În cazul în care parametrul luna este absent sau este considerat invalid, este returnat un mesaj de eroare 
în format JSON.
*/

include_once('database.php');

if (isset($_GET['month']) && is_numeric($_GET['month'])) {
    $selectedMonth = (int)$_GET['month'];

    $sql = "SELECT categorie, SUM(suma) as total FROM tranzactii WHERE tip='cheltuiala' AND data >= DATE_SUB(NOW(), INTERVAL 12 MONTH) AND MONTH(data) = $selectedMonth GROUP BY categorie";

    $result = $mysqli->query($sql);

    $categories = [];
    $amounts = [];

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $categories[] = $row['categorie'];
            $amounts[] = $row['total'];
        }
    }

    $mysqli->close();

    echo json_encode(['totalCheltuieliLunare' => array_sum($amounts), 'categorii' => $categories, 'sume' => $amounts]);
} 
?>
