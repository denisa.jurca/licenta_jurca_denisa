/*Formularul de înregistrare din acest cod JavaScript este supus validării utilizând biblioteca JustValidate. Codul asigură completarea 
corectă a câmpurilor, incluzând maximum 10 caractere pentru nume, o adresă de e-mail validă și disponibilă și o parolă care îndeplinește 
criteriile de complexitate specificate. După validarea cu succes, formularul este apoi trimis.*/

const validation = new JustValidate("#inregistrare");

const errorMessages = {
  required: "Acest câmp este obligatoriu.",
  email: "Introduceți o adresă de email validă.",
  password: "Parola trebuie să conțină minimum opt caractere, cel puțin o literă și o cifră",
  passwordsMatch: "Parolele trebuie să coincidă.",
  emailTaken: "Emailul este deja folosit!"
};

validation
  .addField("#nume", [
    {
      rule: "required",
      errorMessage: errorMessages.required
    },
    {
      validator: (value) => {
        return value.length <= 10;
      },
      errorMessage: "Numele trebuie să aibă maximum 10 caractere."
    }
  ])
  .addField("#email", [
    {
      rule: "required",
      errorMessage: errorMessages.required
    },
    {
      rule: "email",
      errorMessage: errorMessages.email
    },
    {
      validator: (value) => () => {
        return fetch("validare_email.php?email=" + encodeURIComponent(value))
          .then(function (response) {
            return response.json();
          })
          .then(function (json) {
            return json.available;
          });
      },
      errorMessage: errorMessages.emailTaken
    }
  ])
  .addField("#parola", [
    {
      rule: "required",
      errorMessage: errorMessages.required
    },
    {
      rule: "password",
      errorMessage: errorMessages.password
    }
  ])
  .addField("#repetare-parola", [
    {
      validator: (value, fields) => {
        return value === fields["#parola"].elem.value;
      },
      errorMessage: errorMessages.passwordsMatch
    }
  ])
  .onSuccess((event) => {
    document.getElementById("inregistrare").submit();
  });