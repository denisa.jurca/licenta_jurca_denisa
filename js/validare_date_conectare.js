/*Codul JavaScript furnizat are scopul de a valida un formular de conectare. Include criterii specifice atât pentru câmpurile de e-mail,
 cât și pentru parolă. În primul rând, câmpul de e-mail trebuie să fie completat și să conțină o adresă de e-mail validă. În al doilea
rând, trebuie completat și câmpul pentru parolă. Numai când toate aceste cerințe sunt îndeplinite, formularul devine eligibil pentru 
trimitere.*/

const validation = new JustValidate("#formular-conectare");

const errorMessages = {
  required: "Acest câmp este obligatoriu.",
  email: "Introduceți o adresă de email validă.",
};

validation
  .addField("#email", [
    {
      rule: "required",
      errorMessage: errorMessages.required
    },
    {
      rule: "email",
      errorMessage: errorMessages.email
    },
  ])
  .addField("#parola", [
    {
      rule: "required",
      errorMessage: errorMessages.required
    },
  ])
  .onSuccess((event) => {
    document.getElementById("formular-conectare").submit();
  });
