<?php
/*Codul PHP și HTML prezentat aici a fost conceput pentru a genera un meniu special adaptat pentru utilizatorii autentificați, 
oferind o gamă de funcționalități:

Bugetul curent al utilizatorului este determinat prin utilizarea acestui cod PHP, care extrage informații relevante dintr-o bază de 
date. O prezentare concisă a procesului este prezentată mai jos:

Includerea fișierului pentru baza de date se realizează prin utilizarea funcției include_once(), care stabilește conexiunea necesară 
la baza de date.

Pentru a obține suma totală a veniturilor și cheltuielilor din tabelul de tranzacții, sunt executate două interogări distincte, 
utilizând clauza SUM() și aplicând filtre în funcție de tipul tranzacției (venit sau cheltuială).

Recuperarea rezultatelor interogării se realizează prin utilizarea funcției query() a obiectului mysqli, urmată de implementarea 
metodei fetch_assoc(). Rezultatele menționate mai sus sunt stocate ulterior în variabilele desemnate, și anume $venit și $cheltuială.

Procesul de determinare a bugetului curent presupune deducerea cheltuielilor din venituri.

Funcția json_encode() este utilizată pentru a genera un răspuns JSON care conține informații despre bugetul curent, veniturile și 
cheltuielile.

Pentru a închide corect conexiunea la baza de date, se utilizează metoda close() a obiectului mysqli.*/

include_once('database.php');

$mysqli = require __DIR__ . "/database.php";

$sqlVenituri = "SELECT SUM(suma) as total FROM tranzactii WHERE tip='venit'";
$rezultatVenituri = $mysqli->query($sqlVenituri);
$venituri = $rezultatVenituri->fetch_assoc()['total'];

$sqlCheltuieli = "SELECT SUM(suma) as total FROM tranzactii WHERE tip='cheltuiala'";
$rezultatCheltuieli = $mysqli->query($sqlCheltuieli);
$cheltuieli = $rezultatCheltuieli->fetch_assoc()['total'];

$bugetCurent = $venituri - $cheltuieli;

echo json_encode(['bugetCurent' => $bugetCurent, 'venituri' => $venituri, 'cheltuieli' => $cheltuieli]);

$mysqli->close();
?>
