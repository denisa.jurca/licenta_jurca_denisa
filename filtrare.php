<script>
/*Formularul prezentat aici este echipat cu diverse câmpuri care permit utilizatorilor să selecteze criterii specifice, inclusiv 
tipul (fie venituri sau cheltuieli), categoria, suma și data. Prin specificarea unei perioade de timp dorite, utilizatorii pot filtra 
efectiv datele pe baza acestor criterii. Pentru a facilita selectarea datelor în format românesc, sunt folosite bibliotecile Flatpickr, 
în timp ce JavaScript gestionează funcționalitățile interactive, permițând afișarea sau ascunderea formularului de filtrare la clic pe 
o pictogramă.*/
</script>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Meniu</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ro.js"></script>
    <link rel="stylesheet" type="text/css" href="aplicatie_design.css">
</head>
<body>
    <form action="pagina_principala.php" method="GET" id="filtrareForm">
        <div id="afisareFiltrare">
                <h3>Filtrare după:</h3>

                <p>Tip</p>
                <label for="toate">
                    <input type="radio" id="toate" name="tip" value="toate" checked> Toate
                </label>
                <br>
                <label for="venituri">
                    <input type="radio" id="venituri" name="tip" value="venit"> Venituri
                </label>
                <br>
                <label for="cheltuieli">
                    <input type="radio" id="cheltuieli" name="tip" value="cheltuiala"> Cheltuieli
                </label>
                <hr>
                <p>Categorie</p>
                <select id="categorie" name="categorie">
                        <option disabled selected value="">Selectează o categorie</option>
                        <option value="salar">Salar</option>
                        <option value="afacere">Afacere</option>
                        <option value="investitie">Investiție</option>
                        <option value="venit_extra">Venit extra</option>
                        <option value="mancare">Mâncare</option>
                        <option value="restaurant">Restaurant</option>
                        <option value="cumparaturi">Cumpărături</option>
                        <option value="haine">Haine</option>
                        <option value="transport">Transport</option>
                        <option value="combustibil">Combustibil</option>
                        <option value="chirie">Chirie</option>
                        <option value="rata">Rată</option>
                        <option value="internet">Internet</option>
                        <option value="electricitate">Electricitate</option>
                        <option value="apa">Apă</option>
                        <option value="asigurare">Asigurare</option>
                        <option value="abonament">Abonament</option>
                        <option value="sport">Sport</option>
                        <option value="sanatate">Sănătate</option>
                        <option value="divertisment">Divertisment</option>
                        <option value="educatie">Educație</option>
                        <option value="cadouri">Cadouri</option>
                        <option value="vacanta">Vacanță</option>
                        <option value="altceva">Altceva</option>
                </select>
                <hr>
                <p>Sumă</p>
                <input type="number" id="suma" name="suma" step="any" placeholder="Introduceți suma">

                <label for="sub">
                    <input type="radio" id="sub" name="comparatie" value="sub">Sub
                </label>

                <label for="peste">
                    <input type="radio" id="peste" name="comparatie" value="peste">Peste
                </label>
                <hr>
                <p>Dată</p>
                <label for="zi">Zi</label>
                <input type="radio" id="zi" name="optiune_data" value="zi">

                <label for="luna">Lună</label>
                <input type="radio" id="luna" name="optiune_data" value="luna">
            
                <label for="an">An</label>
                <input type="radio" id="an" name="optiune_data" value="an">
                
                <input type="number" id="valoare_data" name="valoare_data" placeholder="Introduceți ziua, luna sau anul">
                <hr>
                <p>Perioadă</p>
                <label for="data_inceput">Din data de:</label>
                <input type="text" id="data_inceput" name="data_inceput" placeholder="Zi/Luna/An">
               
                <label for="data_sfarsit">Până în data de:</label>
                <input type="text" id="data_sfarsit" name="data_sfarsit" placeholder="Zi/Luna/An">
                

            <button type="submit" class="butonFiltrare">Filtrează</button>
        </div>
    </form>

    <div id="filtrarePictograma">
        <img src="filtrare.png" id="filtrare">
    </div>

    <script>
        flatpickr("#data_inceput",{
            dateFormat: "d/m/Y",
            locale: "ro", 
            enableTime: false 
        });

        flatpickr("#data_sfarsit", {
            dateFormat: "d/m/Y",
            locale: "ro"
        });

        var filtrarePictograma=document.getElementById("filtrarePictograma");
        var afisareFiltrare=document.getElementById("afisareFiltrare");
        var filtrare=document.getElementById("filtrare");

        afisareFiltrare.style.left="-330px";

        filtrarePictograma.onclick=function(){
            if(afisareFiltrare.style.left=="-330px"){
                afisareFiltrare.style.left="0";
            }
            else{
                afisareFiltrare.style.left="-330px";
            }
        }

    </script>
</body>
</html>