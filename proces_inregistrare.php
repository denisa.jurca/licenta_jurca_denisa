<?php
/*
Datele transmise prin formularul web sunt primite și procesate de acest script PHP, care include pasul de hashing a parolei înainte de 
a o adăuga la baza de date. Mai jos este o explicație mai succintă:

Pentru a ne asigura că datele sunt transmise prin formularul web, se face o cerere de verificare pentru a confirma că cererea HTTP 
este de tip POST.

În scopul asigurării securității, parola trece printr-un proces de hashing folosind funcția password_hash(), după care este stocată 
ulterior în baza de date.

Procesul de stabilire a unei conexiuni la baza de date presupune utilizarea informațiilor furnizate într-un fișier de configurare.

Procesul de pregătire și execuție a unei interogări SQL implică pregătirea și execuția interogării pentru a introduce date în tabelul 
utilizator.

În cazul în care procesul de inserare dă rezultate de succes, utilizatorul va fi direcționat către o altă pagină web. În schimb, dacă 
adresa de e-mail este deja utilizată sau apare orice altă eroare, utilizatorului i se va prezenta un mesaj de eroare.

Actul de blocare a accesului direct este strict aplicat, prin care orice încercare de acces direct la script este interzisă. În cazul 
în care se face o solicitare HTTP, excluzând metoda POST, va fi prezentat un mesaj corespunzător.
*/
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $parola_hash = password_hash($_POST["parola"], PASSWORD_DEFAULT);

        $mysqli = require __DIR__ . "/database.php";

        $sql = "INSERT INTO utilizator(nume, email, parola_hash) VALUES(?, ?, ?)";

        $stmt = $mysqli->stmt_init();

        if(!$stmt->prepare($sql)){
            die("Eroare SQL: " . $mysqli->error);
        }

        $stmt->bind_param("sss", $_POST["nume"], $_POST["email"], $parola_hash);

        if($stmt->execute()){
            header("Location: conectare.php");
            exit;
        }else{
            if($mysqli->errno === 1062){
                die("Adresa de email este deja folosită.");
            }else{
                die("Eroare: " . $mysqli->error);
            }
        }
    } else {
        die("Accesul direct interzis.");
    }
?>
