<?php
/*Următorul cod PHP și HTML cuprinde o pagină web concepută cu scopul de a genera o proiecție a veniturilor și cheltuielilor financiare.
Mai jos este prezentată o elucidare concisă a funcționalității sale:

Pentru a încorpora instrucțiuni PHP, este necesar să se includa fișierele PHP esențiale care sunt responsabile pentru gestionarea 
sesiunilor, gestionarea bazei de date și administrarea meniurilor.

Procesul de prelucrare a datelor și calcul de predicție implică utilizarea datelor furnizate de utilizator a numărului de luni pentru 
a genera previziuni pentru venituri și cheltuieli. Aceste prognoze sunt derivate din datele stocate în baza de date și sunt calculate 
prin aplicarea tehnicilor de regresie liniară.

Formularul de intrare pentru numărul de luni este un formular HTML care permite utilizatorilor să introducă durata dorită, în luni, 
pentru care intenționează să genereze predicții.

Afișarea predictiilor: Predictiile de venituri și cheltuieli sunt prezentate, ținând cont de numărul specificat de luni. În cazurile 
în care nu există date suficiente pentru a genera predicții precise, va fi afișat un mesaj adecvat.

Pentru a obține o pagină web atractivă și receptivă din punct de vedere vizual, clasele CSS și cadrul Bootstrap sunt folosite în scopuri
de stil.*/

include_once('sesiune.php');
include_once('database.php');
include_once('meniu.php');

$numar_luni = 0;
$predictie_venit = 0;
$predictie_cheltuieli = 0;

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["numar_luni"])) {
    $numar_luni = intval($_POST["numar_luni"]);

    $sql_venit = "SELECT MONTH(data) AS luna, SUM(suma) AS venit_lunar
    FROM tranzactii
    WHERE tip = 'venit'
          AND MONTH(data) IN (
              SELECT DISTINCT MONTH(data)
              FROM tranzactii
              ORDER BY data DESC
          )
    GROUP BY luna
    ORDER BY luna DESC";

    $sql_cheltuieli = "SELECT MONTH(data) AS luna, SUM(suma) AS cheltuiala_lunara
    FROM tranzactii
    WHERE tip = 'cheltuiala'
          AND MONTH(data) IN (
              SELECT DISTINCT MONTH(data)
              FROM tranzactii
              ORDER BY data DESC
          )
    GROUP BY luna
    ORDER BY luna DESC";

    $result_venit = $mysqli->query($sql_venit);
    $result_cheltuieli = $mysqli->query($sql_cheltuieli);

    $venituri = array();
    $cheltuieli = array();

    if ($result_venit->num_rows > 0) {
        while($row = $result_venit->fetch_assoc()) {
            $venituri[] = $row["venit_lunar"];
        }
    }

    if ($result_cheltuieli->num_rows > 0) {
        while($row = $result_cheltuieli->fetch_assoc()) {
            $cheltuieli[] = $row["cheltuiala_lunara"];
        }
    }

    if (count($venituri) >= 1 && count($cheltuieli) >= 1) {

        $n_venit = count($venituri);
        $x_venit = range(1, $n_venit);
        $y_venit = $venituri;
        $slope_intercept_venit = calcul_regresie_liniara($x_venit, $y_venit);
        //Stocarea coeficientilor dreptei de regresie liniara: slope, inercept 
        $slope_venit = $slope_intercept_venit['slope'];//coeficientul de înclinare
        $intercept_venit = $slope_intercept_venit['intercept'];//intersectarea dreptei de regresie cu axa verticală (axa y) în 
                                                               //punctul unde axa x este zero.
        $urmatoarea_luna_venit = $n_venit + $numar_luni;
        $predictie_venit = ($slope_venit * $urmatoarea_luna_venit) + $intercept_venit;

        $n_cheltuieli = count($cheltuieli);
        $x_cheltuieli = range(1, $n_cheltuieli);
        $y_cheltuieli = $cheltuieli;
        $slope_intercept_cheltuieli = calcul_regresie_liniara($x_cheltuieli, $y_cheltuieli);
        $slope_cheltuieli = $slope_intercept_cheltuieli['slope'];
        $intercept_cheltuieli = $slope_intercept_cheltuieli['intercept'];

        $urmatoarea_luna_cheltuieli = $n_cheltuieli + $numar_luni;
        $predictie_cheltuieli = ($slope_cheltuieli * $urmatoarea_luna_cheltuieli) + $intercept_cheltuieli;
    }
}

function calcul_regresie_liniara($x, $y) {
    $n = count($x);
    $x_sum = array_sum($x);
    $y_sum = array_sum($y);
    $xx_sum = 0;
    $xy_sum = 0;

    for($i = 0; $i < $n; $i++) {
        $xy_sum += ($x[$i] * $y[$i]);
        $xx_sum += ($x[$i] * $x[$i]);
    }

    $slope = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));
    $intercept = ($y_sum - ($slope * $x_sum)) / $n;

    return array( 
        'slope'     => $slope,
        'intercept' => $intercept,
    );
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="aplicatie_design.css">
    <title>Predicție Cheltuieli și Venituri</title>
</head>
<body>

<header>
    <img src="logo.png" alt="Logo" class="logo-parte-sus">
</header>

<div class="predictie-container mt-5">
                <div class="row justify-content-center">
                    <div class="titlu-predictie">
                        <h2 class="text-center">
                        <img src="pred.png" alt="Predictie" class="bani-pictograma"> PREDICȚIE CHELTUIELI ȘI VENITURI<span id="pred_venituri_cheltuieli"></span>
                        </h2>
                    </div>
                </div>

    <div class="afisare-predictie mt-4">
                <form method="post" class="predictie-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <label for="numar_luni" class="predictie-form-eticheta">Introduceți numărul de luni </label>
                    <div class="input-container">
                        <input type="number" class="form-control predictie-form-control" name="numar_luni" id="numar_luni" min="1" 
                        required>
                    </div>
                    <div class="col btn-container">
                        <button type="submit" class="btn btn-primary">Realizează Predicție</button>
                    </div>
                </form>
        <div class="container mt-3">
            <div class="row">
                <div class="col">
                <div class="predictie-cadran">
                    <div class="prediction-body">
                        <?php
                        
                            if ($numar_luni > 0) {
                                if ($predictie_venit > 0 && $predictie_cheltuieli > 0) {
                                    echo "<p class='prediction-cadran-text'>Predicția veniturilor pentru următoarele " . $numar_luni . " luni este: " . number_format($predictie_venit, 2) . "</p>";
                                    echo "<p class='prediction-cadran-text'>Predicția cheltuielilor pentru următoarele " . $numar_luni . " luni este: " . number_format($predictie_cheltuieli, 2) . "</p>";
                                } else {
                                    echo "<p class='prediction-cadran-text'>Nu există suficiente date pentru a efectua predicțiile.</p>";
                                }
                            }
                        
                        ?>
    </div>
        </div>
            </div>
                </div>
                    </div>
</div>
</body>
</html>

