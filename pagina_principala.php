<?php
/*Codul furnizat este format din PHP și HTML, conceput pentru a prezenta o pagină web care facilitează gestionarea tranzacțiilor 
financiare. Pagina cuprinde un tabel cuprinzător de tranzacții, însoțit de un grafic comparativ care ilustrează veniturile și 
cheltuielile.

Includerea fișierelor PHP esențiale le cuprinde pe cele necesare pentru gestionarea sesiunilor, a meniurilor și a filtrelor.

Pentru a stabili un aspect bine organizat, pagina HTML aderă la structura convențională care cuprinde elementele antet, corp și subsol.

Încorporarea resurselor externe implică utilizarea bibliotecii Chart.js pentru crearea diagramelor și cadrul Bootstrap pentru stilul 
paginii.

Bugetul actual este afișat prin utilizarea unui script PHP, asigurând actualizări dinamice pentru a reflecta cea mai recentă stare 
financiară.

Tabelul de tranzacții prezintă o descriere cuprinzătoare a informațiilor despre tranzacție, cuprinzând diferite aspecte, cum ar fi 
tipul, categoria, descrierea, suma și data. Aceste date valoroase sunt preluate dintr-o bază de date și redate pe ecran prin 
utilizarea PHP.*/

include_once('sesiune.php');
include_once('meniu.php');
include_once('filtrare.php');
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel tranzactii</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="aplicatie_design.css">
    
</head>
<body>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="cadran-titlu">
                <h2 class="text-center">
                  <img src="bani.png" alt="Bani" class="bani-pictograma"> BUGET CURENT: <span id="buget_curent"></span>
                 </h2>
             </div>
        </div>
    </div>

    <div class="container mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="element">
                    <div class="element-body">
                        <canvas id="diagramaComparativa"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-3">
                <div class="element">
                    <div class="element-body">
                        <h5 class="element-title">
                        Tranzacții</h5>
                        <div class="table-responsive">
                            <div class="table-scroll">
                                <table class="tabel">
                                    <thead>
                                        <tr>
                                            <th>Tipul</th>
                                            <th>Categorie</th>
                                            <th>Descriere</th>
                                            <th>Suma</th>
                                            <th>Data</th>
                                            <th>Acțiune</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                          include_once('database.php');

                                          $mysqli = require __DIR__ . "/database.php";
                    
                                          $tip = isset($_GET['tip']) ? $_GET['tip'] : 'toate';
                                          $categorie = isset($_GET['categorie']) ? $_GET['categorie'] : '';
                                          $suma = isset($_GET['suma']) ? $_GET['suma'] : '';
                                          $comparatie = isset($_GET['comparatie']) ? $_GET['comparatie'] : '';
                                          $optiune_data = isset($_GET['optiune_data']) ? $_GET['optiune_data'] : '';
                                          $valoare_data = isset($_GET['valoare_data']) ? $_GET['valoare_data'] : '';
                                          $data_inceput = isset($_GET['data_inceput']) ? $_GET['data_inceput'] : '';
                                          $data_sfarsit = isset($_GET['data_sfarsit']) ? $_GET['data_sfarsit'] : '';
                        
                                          $sql = "SELECT * FROM tranzactii WHERE 1=1";
                                          
                                          if (!empty($tip) && $tip !== 'toate') {
                                              $sql .= " AND tip='$tip'";
                                          }
                                          
                                          if (!empty($categorie)) {
                                              $sql .= " AND categorie='$categorie'";
                                          }
                                          
                                          if (!empty($suma) && is_numeric($suma) && in_array($comparatie, ['sub', 'peste'])) {
                                              if ($comparatie == 'sub') {
                                                  $sql .= " AND suma < '" . $mysqli->real_escape_string($suma) . "'";
                                              } else if ($comparatie == 'peste'){
                                                  $sql .= " AND suma > '" . $mysqli->real_escape_string($suma) . "'";
                                              }
                                          }
                                          
                                          if (!empty($optiune_data) && !empty($valoare_data)) {
                                              if ($optiune_data === 'zi') {
                                                  $sql .= " AND DAY(data) = '$valoare_data'";
                                              } elseif ($optiune_data === 'luna') {
                                                  $sql .= " AND MONTH(data) = '$valoare_data'";
                                              } elseif ($optiune_data === 'an') {
                                                  $sql .= " AND YEAR(data) = '$valoare_data'";
                                              }
                                          }
                                          
                                          if (!empty($data_inceput) && !empty($data_sfarsit)) {
                                            $data_inceput_obj = DateTime::createFromFormat('d/m/Y', $data_inceput);
                                            $data_inceput_formatata = $data_inceput_obj ? $data_inceput_obj->format('Y-m-d') : '';
                                            
                                            $data_sfarsit_obj = DateTime::createFromFormat('d/m/Y', $data_sfarsit);
                                            $data_sfarsit_formatata = $data_sfarsit_obj ? $data_sfarsit_obj->format('Y-m-d') : '';
                                            
                                            $sql .= " AND data BETWEEN '$data_inceput_formatata' AND '$data_sfarsit_formatata'";
                                        }
                                        
                                          
                                          $sql .= " ORDER BY id";
                                          
                                          $result = $mysqli->query($sql);
                                          
                                          if ($result->num_rows > 0) {
                                              $rows = [];
                                              while ($row = $result->fetch_assoc()) {
                                                  $rows[] = $row;
                                              }              
                                                            
                                                                for ($i = count($rows) - 1; $i >= 0; $i--) {
                                                                    echo "<tr>";
                                                                    echo "<td class='" . ($rows[$i]["tip"] == 'venit' ? 'text-venit' : 'text-cheltuiala') . "'>" . $rows[$i]["tip"] . "</td>";
                                                                    echo "<td>" . $rows[$i]["categorie"] . "</td>";
                                                                    echo "<td>" . $rows[$i]["descriere"] . "</td>";
                                                                    if ($rows[$i]["tip"] == 'venit') {
                                                                        echo "<td class='text-venit'>+" . $rows[$i]["suma"] . "</td>";
                                                                    } else {
                                                                        echo "<td class='text-cheltuiala'>-" . $rows[$i]["suma"] . "</td>";
                                                                    }
                                                                    echo "<td>" . date('d/m/Y', strtotime($rows[$i]["data"])) . "</td>";
                                                                    echo "<td><button class='buton-stergere' onclick='stergereTranzactie(" . $rows[$i]["id"] . ")'>Șterge</button></td>";
                                                                    echo "</tr>";
                                                                }

                                                            }
                          
                                                              $mysqli->close();
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        function stergereTranzactie(idTranzactie) {
            if (confirm("Sigur doriți să ștergeți această tranzacție?")) {
                fetch('proces_stergere.php', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    body: 'idTranzactie=' + idTranzactie,
                })
                .then(response => response.json())
                .then(data => {
                    if (data.success) {
                        alert('Tranzacția ștearsă cu succes!');
                        location.reload(); 
                    } else {
                        alert('Eroare la ștergere.');
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                });
            }
        }

        function actualizareBugetCurent() {
            fetch('obtine_buget_curent.php')
                .then(response => response.json())
                .then(data => {
                    document.getElementById('buget_curent').innerText =formatare_numar(data.bugetCurent, 2);
                    desenareDiagrama(data.venituri, data.cheltuieli);
                    
                })
                .catch(error => {
                    console.error('Error:', error);
                });
        }

        function desenareDiagrama(venituri, cheltuieli) {
            var ctx = document.getElementById('diagramaComparativa').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ['Venituri', 'Cheltuieli'],
                    datasets: [{
                        data: [venituri, cheltuieli],
                        backgroundColor: ['rgba(0, 149, 199, 1)', 'rgba(255, 20, 147, 0.8)'],
                        borderColor: ['rgba(0, 141, 188, 1)', 'rgba(212, 66, 149, 0.9)'],
                        borderWidth: 3
                    }]
                },
                options: {
                     responsive: true,
                     maintainAspectRatio: false
                }
            });
        }

        function formatare_numar(numar, zecimale, punctZecimal, separatorMii) {
            numar = parseFloat(numar);
            if (!zecimale) zecimale = 0;
            if (!punctZecimal) punctZecimal = '.';
            if (!separatorMii) separatorMii = ',';

            var numarRotunjit = Math.pow(10, zecimale);
            numar = Math.round(numar * numarRotunjit) / numarRotunjit;

            var parti = numar.toString().split(".");
            parti[0] = parti[0].replace(/\B(?=(\d{3})+(?!\d))/g, separatorMii);

            return parti.join(punctZecimal);
        }

        actualizareBugetCurent();

    </script>
    
    <footer>
        <img src="logo.png" alt="Logo" class="logo-stanga-jos">
    </footer>
</body>
</html>