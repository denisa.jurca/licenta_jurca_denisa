<?php 
/*
Pagina web în cauză servește ca o platformă de gestionare a veniturilor, utilizând atât codul PHP, cât și codul HTML. Permiteți-mi să 
ofer o explicație concisă a funcționalității sale:

Include instrucțiuni PHP prin includerea fișierelor PHP necesare care facilitează gestionarea sesiunilor și a meniurilor.

Declarația DOCTYPE și secțiunea de head a unei pagini web servesc la definirea tipului de document și pentru a încorpora resurse 
esențiale, cum ar fi fișierele CSS și JavaScript, împreună cu biblioteca Chart.js pentru redarea diagramei.

Elementele container și afișaj joacă un rol inportant în definirea titlului paginii, precum și secțiunile dedicate diagramei și 
introducerii de noi venituri.

Formularul pentru adăugarea venitului este un formular HTML care permite utilizatorilor să introducă informații cu privire la o nouă 
sursă de venit, care este apoi trimisă unui script PHP pentru a fi procesată ulterior.

În domeniul manipulării formularelor, JavaScript servește scopului validării și formatării datelor înainte de transmiterea acestora 
către scriptul PHP de procesare.

Utilizarea funcțiilor JavaScript servește ca scop al actualizării și redării diagramei, cuprinzând funcțiile responsabile pentru 
obținerea veniturilor totale și desenarea diagramei prin implementarea Chart.js. În consecință, acest lucru permite ca datele și 
diagrama să fie actualizate dinamic în timp real.

Scriptul PHP conceput pentru prelucrarea datelor îndeplinește câteva funcții esențiale, inclusiv primirea intrărilor, validarea 
acesteia, adăugarea acesteia în baza de date și, ulterior, redirecționarea utilizatorului către pagina principală.
*/ 

include_once('sesiune.php');
include_once('meniu.php'); 
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Total Venituri și Diagramă</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ro.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="aplicatie_design.css">
</head>
<body>
    
<header>
    <img src="logo.png" alt="Logo" class="logo-parte-sus">
</header>

<div class="container2 mt-5">
        <div class="row justify-content-center">
            <div class="titlu-pag-adaugare">
                <h2 class="text-center">
                  <img src="ven-bani.png" alt="Venituri_Bani" class="bani-pictograma"> TOTAL VENITURI: <span id="total_venituri"></span>
                 </h2>
             </div>
        </div>

    <div id="componente_pag" class="mt-4">
        <div id="diagrama_container" class="mb-4">
            <canvas id="diagramaTip"></canvas>
        </div>

        <div id="intrVenituriCheltuieli" class="mb-4">
           <h3>Introdu venit nou</h3>
            <form method="post" class="adaugare-form">
                <div class="grup">
                    <label for="data">Data</label>
                    <input type="text" class="form-control" id="data" placeholder="Zi/Lună/An">
                </div>
                <div class="grup">
                    <label for="categorie">Categorie</label>
                    <select class="form-control" id="categorie">
                        <option disabled selected value="">Selectează o categorie</option>
                        <option value="salar">Salar</option>
                        <option value="afacere">Afacere</option>
                        <option value="investitie">Investiție</option>
                        <option value="venit_extra">Venit extra</option>
                        <option value="altceva">Altceva</option>
                    </select>
                </div>
                <div class="grup">
                    <label for="suma">Suma</label>
                    <input type="number" class="form-control" placeholder="Adaugă sumă" id="suma">
                </div>
                <div class="grup">
                    <label for="descriere">Descriere</label>
                    <input type="text" class="form-control" placeholder="Adaugă descriere" id="descriere">
                </div>
                <input type="hidden" name="tip" value="venit">
                <button type="submit" class="btn btn-primary" id="button">Adaugă venit</button>
            </form>
        </div>
    </div>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script>
            flatpickr("#data", {
                dateFormat: "d/m/Y",
                locale: "ro", 
                enableTime: false
            });
            document.getElementById("button").addEventListener("click", function (event) {
            event.preventDefault();

            var dataInput = document.getElementById("data");
            var dataPrimita = dataInput.value;
            var dataSplit = dataPrimita.split('/'); 
            var dataFormatata = dataSplit[2] + '-' + dataSplit[1] + '-' + dataSplit[0]; 

            var categorie = document.getElementById("categorie").value;
            var suma = document.getElementById("suma").value;
            var descriere = document.getElementById("descriere").value;
            var tip = document.querySelector('input[name="tip"]').value;

            if (dataInput.value.trim() === '' || categorie.trim() === '' || suma.trim() === '' || descriere.trim() === '') {
                alert('Toate câmpurile trebuie completate.');
                return;
            }
             var formData = new FormData();
             formData.append("data", dataFormatata);
             formData.append("categorie", categorie);
             formData.append("suma", suma);
             formData.append("descriere", descriere);
             formData.append("tip", tip);
        
                fetch('proces_tranzactie.php', {
                    method: 'POST',
                    body: formData,
                })
                .then(response => response.json())
                .then(data => {
                    if (data.message) {
                        alert(data.message);  
                        window.location.href = 'pagina_principala.php';  
                    } else if (data.error) {
                        alert(data.error); 
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                });
              
            });
       
        function actualizareVenitTotal() {
            fetch('obtine_total_venituri.php')
                .then(response => response.json())
                .then(data => {
                    document.getElementById('total_venituri').innerText = numar_formatat(data.totalVenituri, 2);
                    desenareDiagramaVenituri(data.categorii, data.sume);
                })
                .catch(error => {
                    console.error('Error:', error);
                });
        }

        function desenareDiagramaVenituri(categorii, sume) {
            var ctx = document.getElementById('diagramaTip').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: categorii,
                    datasets: [{
                        data: sume,
                        backgroundColor: [
                            'rgba(0, 149, 199, 1)',
                            'rgba(76, 199, 0, 1)',
                            'rgba(196, 199, 0, 1)',
                            'rgba(199, 146, 0, 1)',
                            'rgba(199, 0, 106, 1)'
                        ],
                        borderColor: [
                            'rgba(0, 149, 199, 1)',
                            'rgba(76, 199, 0, 1)',
                            'rgba(196, 199, 0, 1)',
                            'rgba(199, 146, 0, 1)',
                            'rgba(199, 0, 106, 1)'
                        ],
                        borderWidth: 3
                    }]
                },
                options: {
                     responsive: true,
                     maintainAspectRatio: false
                }
            });
        }

        function numar_formatat(numar, zecimale, separatorZecimal, separatorMii) {
            numar = parseFloat(numar);
            if (!zecimale) zecimale = 0;
            if (!separatorZecimal) separatorZecimal = '.';
            if (!separatorMii) separatorMii = ',';

            var numarRotunjit = Math.pow(10, zecimale);
            numar = Math.round(numar * numarRotunjit) / numarRotunjit;

            var parti = numar.toString().split(".");
            parti[0] = parti[0].replace(/\B(?=(\d{3})+(?!\d))/g, separatorMii);

            return parti.join(separatorZecimal);
        }

        actualizareVenitTotal();
    </script>
    
</body>
</html>
