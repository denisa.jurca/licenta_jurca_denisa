<?php
/*Următorul cod, scris în PHP și HTML, servește ca pagină de conectare a utilizatorului. 
Iată o prezentare concisă a funcționalității sale:

În urma completării și trimiterii formularului de către utilizator, se efectuează o verificare de autentificare prin acest cod pentru 
a verifica existența adresei de e-mail în baza de date și pentru a se asigura că parola furnizată se potrivește cu adresa de e-mail 
asociată.

În cazul în care apar erori în timpul procesului de conectare, cum ar fi introducerea unei parole incorecte sau o adresă de e-mail 
inexistentă, sistemul va genera și afișa mesaje de eroare care sunt relevante pentru problema specifică.

Bootstrap este responsabil pentru designul elegant al formularului, în timp ce JavaScript îmbunătățește interactivitatea prin afișarea 
sau ascunderea dinamică a unei pictograme lângă câmpurile de introducere pe baza conținutului acestora.

În concluzie, această pagină web oferă utilizatorilor posibilitatea de a-și accesa conturile existente utilizând adresa de e-mail și 
parola corecte, oferind în același timp gestionarea mesajelor de eroare și o interfață captivantă.
*/

$mesaj_invalidare = "";

if($_SERVER["REQUEST_METHOD"] === "POST"){

    $mysql=require __DIR__ . "/database.php";

    $sql=sprintf("SELECT * FROM utilizator WHERE email='%s'",$mysqli->real_escape_string($_POST["email"]));

    $result= $mysqli->query($sql);

    $utilizator= $result->fetch_assoc();

    if($utilizator){
        if(password_verify($_POST["parola"],$utilizator["parola_hash"])){

            session_start();

            session_regenerate_id();

            $_SESSION["utilizator_id"]=$utilizator["id"];

            header("Location:pagina_principala.php");
            exit;
            
        } else {
            $mesaj_invalidare = "Parolă incorectă";
        }
    } else {
        $mesaj_invalidare = "Email inexistent";
    }
}
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <title>Conectare</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://unpkg.com/just-validate@latest/dist/just-validate.production.min.js" defer></script>
    <script src="js/validare_date_conectare.js" defer></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="inregistrare_conectare_design.css" rel="stylesheet">
</head>
<body>

    <header>
        <img src="logo.png" alt="Logo" class="logo-parte-sus">
    </header>

    <div class="container">
        <form method="post" id="formular-conectare">
            <div class="utilizator-pictograma">
                <img src="utilizator-inregistrare-conectare.png" alt="Imagine deasupra formularului">
            </div>
            <h1 class="text-center mb-4">Conectare</h1>
            <?php if(!empty($mesaj_invalidare)):?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $mesaj_invalidare; ?>
                </div>
            <?php endif; ?>
            <div class="mb-3">
                <label for="email" class="eticheta-form">Email</label>
                <input type="email" class="form-control form-control-email-pictograma" id="email" name="email">
            </div>

            <div class="mb-3">
                <label for="parola" class="eticheta-form">Parolă</label>
                <input type="password" class="form-control form-control-parola-pictograma" id="parola" name="parola">
            </div>

            <button type="submit" class="buton buton-inregistrare-conectare">Conectați-vă</button>
            <p class="mt-3 text-center">Nu aveți cont? <a href="inregistrare.html">Înregistrați-vă</a></p>
        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        const campuriIntroducereDate = document.querySelectorAll('.form-control');

        campuriIntroducereDate.forEach(input => {
        
            input.addEventListener('click', function() {
                this.classList.add('pictograma-ascunsa');
            });
            
            input.addEventListener('keyup', function() {
                if (this.value.trim() !== '') {
                    this.classList.add('pictograma-ascunsa');
                } else {
                    this.classList.remove('pictograma-ascunsa');
                }
            });
        });
    </script>
</body>
</html>
