<?php 
/*
Următorul cod PHP și HTML este utilizat pentru a construi o pagină web dedicată gestionării cheltuielilor. O prezentare concisă 
a funcționalității sale este prezentată mai jos:

Pentru a asigura funcționalitatea corespunzătoare, este necesar să se încorporeze fișierele PHP esențiale, în special cele responsabile
cu gestionarea sesiunilor și a meniurilor, prin utilizarea funcției include_once.

Structura HTML cuprinde definirea cadrului fundamental al paginii web, cuprinzând componente importante, cum ar fi antetul și corpul.

Procesul de importare a resurselor externe implică încorporarea de biblioteci externe, cum ar fi Chart.js în scopul desenării 
diagramelor și Flatpickr pentru facilitarea selecției datelor.

Adăugarea formularului de cheltuieli: Acest formular permite utilizatorilor să introducă informații cu privire la o nouă cheltuială.
Acesta cuprinde secțiuni pentru dată, categorie, sumă și descriere.

În timpul interacțiunii cu baza de date, la apăsarea butonului de adăugare cheltuieli, valorile introduse sunt transmise unui 
script PHP, și anume proces_tranzactie.php, pentru a fi supuse procesării și ulterior documentate în baza de date.

Solicitarea serverului să recupereze cheltuielile totale și să genereze o diagramă circulară care să ilustreze distribuția cheltuielilor
este procesul de actualizare și afișare a cheltuielilor totale. Această actualizare are loc la intervale regulate, precum și în timpul 
încărcării inițiale a paginii.*/
include_once('sesiune.php');
include_once('meniu.php'); 
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Total Cheltuieli și Diagramă</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr/dist/l10n/ro.js"></script>
    <link rel="stylesheet" type="text/css" href="aplicatie_design.css">
</head>
<body>

<header>
    <img src="logo.png" alt="Logo" class="logo-parte-sus">
</header>

<div class="container2 mt-5">
        <div class="row justify-content-center">
            <div class="titlu-pag-adaugare">
                <h2 class="text-center">
                  <img src="chelt-bani.png" alt="Cheltuieli_Bani" class="bani-pictograma"> TOTAL CHELTUIELI: <span id="total_cheltuieli"></span>
                 </h2>
             </div>
        </div>
    
    <div id="componente_pag" class="mt-4">
        <div id="diagrama_container" class="mb-4">
            <canvas id="diagramaTip"></canvas>
        </div>

        <div id="intrVenituriCheltuieli" class="mb-4">
          <h3>Introdu cheltuială nouă</h3>
            <form method="post" class="adaugare-form">
                <div class="grup">
                    <label for="data">Data</label>
                    <input type="text" class="form-control" id="data" placeholder="Zi/Lună/An">
                </div>

                <div class="grup">
                    <label for="categorie">Categorie</label>
                    <select class="form-control" id="categorie">
                        <option disabled selected value="">Selectează o categorie</option>
                        <option value="mancare">Mâncare</option>
                        <option value="restaurant">Restaurant</option>
                        <option value="cumparaturi">Cumpărături</option>
                        <option value="haine">Haine</option>
                        <option value="transport">Transport</option>
                        <option value="combustibil">Combustibil</option>
                        <option value="chirie">Chirie</option>
                        <option value="rata">Rată</option>
                        <option value="internet">Internet</option>
                        <option value="electricitate">Electricitate</option>
                        <option value="apa">Apă</option>
                        <option value="asigurare">Asigurare</option>
                        <option value="abonament">Abonament</option>
                        <option value="sport">Sport</option>
                        <option value="sanatate">Sănătate</option>
                        <option value="divertisment">Divertisment</option>
                        <option value="educatie">Educație</option>
                        <option value="cadouri">Cadouri</option>
                        <option value="vacanta">Vacanță</option>
                        <option value="altceva">Altceva</option>
                    </select>
                </div>
                <div class="grup">
                    <label for="suma">Suma</label>
                    <input type="number" class="form-control" placeholder="Suma" id="suma">
                </div>
                <div class="grup">
                    <label for="descriere">Descriere</label>
                    <input type="text" class="form-control" placeholder="Descriere" id="descriere">
                </div>
                <input type="hidden" name="tip" value="cheltuiala">
                <button type="submit" class="btn btn-primary" id="button">Adaugă cheltuială</button>
            </form>
        </div>
    </div>
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script>
            flatpickr("#data", {
                dateFormat: "d/m/Y",
                locale: "ro", 
                enableTime: false
            });
            
            document.getElementById("button").addEventListener("click", function (event) {
            event.preventDefault();

            var dataInput = document.getElementById("data");
            var dataPrimita = dataInput.value;
            var dataSplit = dataPrimita.split('/'); 
            var dataFormatata = dataSplit[2] + '-' + dataSplit[1] + '-' + dataSplit[0]; 

            var categorie = document.getElementById("categorie").value;
            var suma = document.getElementById("suma").value;
            var descriere = document.getElementById("descriere").value;
            var tip = document.querySelector('input[name="tip"]').value;

            if (dataInput.value.trim() === '' || categorie.trim() === '' || suma.trim() === '' || descriere.trim() === '') {
                alert('Toate câmpurile trebuie completate.');
                return;
            }

             var formData = new FormData();
             formData.append("data", dataFormatata);
             formData.append("categorie", categorie);
             formData.append("suma", suma);
             formData.append("descriere", descriere);
             formData.append("tip", tip);
        
                fetch('proces_tranzactie.php', {
                    method: 'POST',
                    body: formData,
                })
                .then(response => response.json())
                .then(data => {
                    if (data.message) {
                        alert(data.message);  
                        window.location.href = 'pagina_principala.php';  
                    } else if (data.error) {
                        alert(data.error); 
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                });
            });

        function actualizareCheltuieliTotale() {
            fetch('obtine_total_cheltuieli.php')
                .then(response => response.json())
                .then(data => {
                    document.getElementById('total_cheltuieli').innerText = numar_formatat(data.totalCheltuieli, 2);
                    desenareDiagramaCheltuieli(data.categorii, data.sume);
                })
                .catch(error => {
                    console.error('Error:', error);
                });
        }

        function desenareDiagramaCheltuieli(categorii, sume) {
            var ctx = document.getElementById('diagramaTip').getContext('2d');
            var backgroundColors = [
                        'rgba(161, 0, 0, 1)',      
                        'rgba(176, 59, 0, 1)',     
                        'rgba(176, 85, 0, 1)',
                        'rgba(187, 115, 0, 1)',
                        'rgba(187, 153, 0, 1)',
                        'rgba(171, 187, 0, 1)',   
                        'rgba(176, 215, 0, 1)',
                        'rgba(125, 187, 0, 1)', 
                        'rgba(53, 158, 0, 1)',     
                        'rgba(0, 150, 68, 1)',       
                        'rgba(0, 160, 141, 1)',      
                        'rgba(0, 150, 150, 1)',      
                        'rgba(0, 72, 149, 1)',     
                        'rgba(41, 0, 138, 1)',      
                        'rgba(75, 39, 146, 1)',      
                        'rgba(149, 0, 149, 1)',     
                        'rgba(164, 0, 139, 1)', 
                        'rgba(173, 0, 112, 1)',      
                        'rgba(164, 0, 79, 1)', 
                        'rgba(159, 0, 50, 1)'     

                    ];

            var borderColors = backgroundColors.map(color => color.replace('1', '1'));

            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: categorii,
                    datasets: [{
                        data: sume,
                        backgroundColor: backgroundColors,
                        borderColor: borderColors,
                        borderWidth: 5
                    }]
                },
                options: {
                     responsive: true,
                     maintainAspectRatio: false
                }
            });
        }

        function numar_formatat(numar, zecimale, separatorZecimal, separatorMii) {
            numar = parseFloat(numar);
            if (!zecimale) zecimale = 0;
            if (!separatorZecimal) separatorZecimal = '.';
            if (!separatorMii) separatorMii = ',';

            var numarRotunjit = Math.pow(10, zecimale);
            numar = Math.round(numar * numarRotunjit) / numarRotunjit;

            var parti = numar.toString().split(".");
            parti[0] = parti[0].replace(/\B(?=(\d{3})+(?!\d))/g, separatorMii);

            return parti.join(separatorZecimal);
        }

        actualizareCheltuieliTotale();
    </script>
</body>
</html>
