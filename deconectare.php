<?php
/*Scopul acestui fișier PHP este să întrerupă conexiunea utilizatorului prin eliminarea sesiunii actuale și să-l redirecționeze către 
pagina de autentificare (conectare.php). Mai jos se poate găsi un rezumat concis al codului:
    
Sesiunea în PHP poate fi începută și variabilele de sesiune pot fi accesate prin utilizarea funcției session_start().
Apelul funcției session_destroy() elimină toate informațiile stocate în sesiune.
După aceea, prin intermediul apelului funcției header(), utilizatorul este direcționat către pagina de conectare (conectare.php).
Comanda exit este utilizată pentru a opri imediat executarea scriptului după redirecționare.
Prin această acțiune, utilizatorul este delogat și redirecționat către ecranul unde poate introduce datele de autentificare.*/ 

session_start();

session_destroy();

header("Location: conectare.php");
exit;
?>
