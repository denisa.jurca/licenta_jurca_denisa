<?php 
/*Codul PHP furnizat generează o pagină web de bază care conține un formular pentru introducerea unei limite de cheltuieli și o zonă 
desemnată pentru prezentarea rezultatelor în formă tabelară. 

Se iclud fișierelor PHP pentru sesiune și meniu, urmată de începerea documentului HTML.

Formularul de introducere a limitei de cheltuieli este prezentat utilizatorilor, permițându-le să introducă limita de cheltuieli dorită.
Acest formular este apoi redirecționat către scriptul de procesare, obtine_cheltuieli_neobisnuite.php, pentru manipulare și execuție 
ulterioară.

Afișarea rezultatelor este facilitată prin utilizarea unui element div, care are scopul de a prezenta rezultatele procesării și 
încorporează un tabel generat dinamic implementat cu JavaScript.

În domeniul programării JavaScript, este implementat un script pentru a asculta în mod activ apariția evenimentului de trimitere a 
formularului. La detectarea acestui eveniment, scriptul procedează la inițierea unei solicitări către un anumit punct final, 
obtine_cheltuieli_neobisnuite.php, cu intenția de a prelua date JSON. Ulterior, datele dobândite sunt prezentate meticulos într-un 
format tabelar în limitele rezultatelor div.*/

include_once('sesiune.php');
include_once('meniu.php'); 
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cheltuieli neașteptate pentru anul trecut</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="aplicatie_design.css">
</head>
<body>

<header>
    <img src="logo.png" alt="Logo" class="logo-parte-sus">
</header>

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="titlu-cheltuieli-neobisnuite">
            <h2 class="text-center">
                <img src="chelt-neob.png" alt="Cheltuieli neobisnuite" class="bani-pictograma"> Cheltuieli neobișnuite pentru anul trecut
            </h2>
        </div>
    </div>
    <div class="cadran-cheltuieli-neobisnuite mt-4 align-items-center text-center">
        <form id="sumaForm" method="post" action="obtine_cheltuieli_neobisnuite.php">
            <div class="input-group mb-3">
                <span class="input-group-text">Specificați limita de cheltuieli</span>
                <input type="number" class="form-control" id="suma" name="suma" required>
            </div>
            <button type="submit" class="buton butonLimita">Trimite</button>
        </form>
    
        <div id="cadranRezultate" class="row justify-content-center mt-5">
            <div class="col-md-8 cadran-rezultate d-flex justify-content-center" id="rezultate"></div>
        </div>

    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        document.getElementById("sumaForm").addEventListener("submit", function (event) {
            event.preventDefault(); 

            var formData = new FormData(this); 
            fetch(this.getAttribute("action"), {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                if (data.error) {
                    document.getElementById('rezultate').innerHTML = "<p class='text-danger'>" + data.error + "</p>"; 
                } else {
                    var html = "<table class='tabelCheltNeob'>";
                    html += "<thead><tr><th scope='col'>Luna</th><th scope='col'>Total cheltuieli</th><th scope='col'>Cheltuiala maximă</th><th scope='col'>Descriere</th><th scope='col'>Sumă</th></tr></thead>";
                    html += "<tbody>";
                    data.forEach(function (item) {
                        html += "<tr>";
                        html += "<td>" + item.luna + "</td>";
                        html += "<td>" + item.total_cheltuieli + "</td>";
                        html += "<td>" + item.categorie_maxima + "</td>";
                        html += "<td>" + item.descriere_maxima + "</td>";
                        html += "<td>" + item.suma_maxima + "</td>";
                        html += "</tr>";
                    });
                    html += "</tbody>";
                    html += "</table>";
                    document.getElementById('rezultate').innerHTML = html; 
                }
            })
            .catch(error => {
                console.error('Error:', error);
                document.getElementById('rezultate').innerHTML = "<p class='text-danger'>Eroare la solicitare.</p>"; 
            });
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
