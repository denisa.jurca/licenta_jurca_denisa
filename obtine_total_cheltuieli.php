<?php
/*
Codul PHP furnizat efectuează o interogare în baza de date pentru a obține cheltuielile totale pentru fiecare categorie și apoi 
prezintă rezultatele sub forma unui obiect JSON. O explicație concisă a codului este următoarea:

Pentru a stabili o conexiune la baza de date, este inclusă conexiunea utilizând fișierul 
database.php.

O interogare de bază de date este construită folosind SQL pentru a calcula cheltuielile totale pentru fiecare categorie din tabelul 
de tranzacții, concentrându-se în mod special pe intrările clasificate "cheltuieli". 

Executarea interogării duce la stocarea rezultatelor obţinute în variabila $result desemnată.

Procesul de extragere a datelor presupune revizuirea sistematică a rândurilor rezultatului obținut și preluarea informațiilor,
respectiv categoriile și sumele corespunzătoare de cheltuieli pentru fiecare categorie. Aceste date sunt apoi stocate 
în matrice distincte.

După finalizarea recuperării datelor necesare, se închide conexiunea la baza de date pentru a elibera eficient 
resursele asociate.

Rezultatul emis include conversia datelor în format JSON.*/
include_once('database.php');

$mysqli = require __DIR__ . "/database.php";

$sql = "SELECT SUM(suma) AS totalCheltuieli, categorie FROM tranzactii WHERE tip = 'cheltuiala' GROUP BY categorie";
$result = $mysqli->query($sql);

$categories = [];
$amounts = [];

while ($row = $result->fetch_assoc()) {
    $categories[] = $row['categorie'];
    $amounts[] = $row['totalCheltuieli'];
}

$mysqli->close();

echo json_encode(['totalCheltuieli' => array_sum($amounts), 'categorii' => $categories, 'sume' => $amounts]);
?>
