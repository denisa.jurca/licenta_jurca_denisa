<?php
/*Funcția de verificare a sesiunii verifică prezența unei sesiuni active pentru utilizator. În cazul în care se stabilește o sesiune, 
detaliile relevante ale utilizatorului sunt preluate din baza de date și prezentate în meniu.

Afișarea meniului în HTML este responsabilă pentru generarea unui meniu care include link-uri către diferite pagini din aplicație, 
cum ar fi tranzacții, venituri, cheltuieli și multe altele. Legăturile sunt activate sau dezactivate dinamic pe baza paginii curente 
care este accesată, utilizând variabila $pagina_curenta.

JavaScript permite implementarea interactivității sub forma unei pictograme pe care se poate face clic care deschide și închide meniul. 

Funcționalitatea de deconectare este accesibilă printr-un buton de deconectare situat în partea de jos a meniului. La apăsarea 
butonului, se declanșează un formular, care la rândul său trimite date către script-ul deconectare.php pentru procesare.*/

if(isset($_SESSION["utilizator_id"])){
    $mysqli = require __DIR__ . "/database.php";

    $sql = "SELECT * FROM utilizator WHERE id={$_SESSION["utilizator_id"]}";
    $result = $mysqli->query($sql);
    $utilizator = $result->fetch_assoc();
}
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Meniu</title>
    <link rel="stylesheet" type="text/css" href="aplicatie_design.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>
    <div id="afisareMeniu">
        <div id="utilizatorCadran">
            <img src="profil.png" alt="Pictograma Utilizator">
            <p id="utilizatorNume"><?php echo $utilizator['nume']; ?></p>
        </div>
        <nav>
            <ul>
                <?php
                $pagina_curenta = basename($_SERVER['PHP_SELF'], ".php");
                ?>

                <li <?php if ($pagina_curenta == 'pagina_principala') echo 'class="activ"'; ?>><a href="pagina_principala.php">Tranzacții</a></li>
                <li <?php if ($pagina_curenta == 'pagina_venituri') echo 'class="activ"'; ?>><a href="pagina_venituri.php">Venituri</a></li>
                <li <?php if ($pagina_curenta == 'pagina_cheltuieli') echo 'class="activ"'; ?>><a href="pagina_cheltuieli.php">Cheltuieli</a></li>
                <li <?php if ($pagina_curenta == 'cheltuieli_lunare') echo 'class="activ"'; ?>><a href="cheltuieli_lunare.php">Cheltuieli lunare</a></li>
                <li <?php if ($pagina_curenta == 'cheltuieli_neobisnuite') echo 'class="activ"'; ?>><a href="cheltuieli_neobisnuite.php">Cheltuieli neobișnuite</a></li>
                <li <?php if ($pagina_curenta == 'comparatie_venituri_cheltuieli') echo 'class="activ"'; ?>><a href="comparatie_venituri_cheltuieli.php">Comparare între venituri și cheltuieli</a></li>
                <li <?php if ($pagina_curenta == 'predictie') echo 'class="activ"'; ?>><a href="predictie.php">Predicție venituri și cheltuieli</a></li>
            </ul>
            <form id="formularDeconectare" action="deconectare.php" method="post">
                <button type="submit" id="butonDeconectare">Deconectare</button>
            </form>

        </nav>
    </div>

    <div id="meniuPictograma">
        <img src="meniu.png" id="meniu">
    </div>

    <script>
        var meniuPictograma=document.getElementById("meniuPictograma");
        var afisareMeniu=document.getElementById("afisareMeniu");
        var meniu=document.getElementById("meniu");

        afisareMeniu.style.right="-330px";

        meniuPictograma.onclick=function(){
            if(afisareMeniu.style.right=="-330px"){
                afisareMeniu.style.right="0";
            }
            else{
                afisareMeniu.style.right="-330px";
            }
        }
       
        document.getElementById('formularDeconectare').addEventListener('submit', function(event) {
            event.preventDefault(); 

            this.submit();
        });

    </script>
