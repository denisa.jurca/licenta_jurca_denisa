<?php
/*
Procesul de primire și validare a datelor trimise prin metoda POST în acest script PHP este foarte important înainte ca acestea să 
poată fi adăugate cu succes la baza de date. Iata mai jos o explicație concisă a modului în care se realizează acest lucru:

Verificarea metodei de solicitare verifică dacă cererea HTTP este clasificată ca tip POST, asigurând astfel că transmiterea datelor 
are loc prin formularul web.

Validarea datelor presupune verificarea faptului că toate câmpurile obligatorii sunt completate și că tipul tranzacției 
(venit sau cheltuială) este corect. Dacă se găsesc discrepanțe, va fi returnat un mesaj de eroare în format JSON.

Datele sunt pregătite și formatate în conformitate cu formatul specificat pentru baza de date. Fișierul database.php conține 
informațiile necesare pentru a stabili o conexiune cu baza de date.

Efectuarea execuției interogării de inserare SQL implică pregătirea unei instrucțiuni SQL special concepută pentru a insera date în 
tabelul de "tranzacții" desemnat. Valorile care trebuie introduse sunt legate în siguranță la parametrii de interogare corespunzători, 
asigurând integritatea operațiunii. Ulterior, interogarea este executată și, la finalizarea cu succes, este returnat un mesaj de succes 
în format JSON. În schimb, în ​​cazul unei operații de inserare nereușite, este generat și returnat un mesaj de eroare.

La executarea interogării, este necesar să se închida atât declarația pregătită, cât și conexiunea la baza de date pentru a elibera 
resursele alocate.*/

include_once('database.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (
        empty($_POST["data"]) ||
        empty($_POST["categorie"]) ||
        !isset($_POST["suma"]) ||
        trim($_POST["suma"]) === "" ||
        empty($_POST["descriere"]) ||
        !isset($_POST["tip"]) ||
        !in_array($_POST["tip"], ["venit", "cheltuiala"])
    ) {
        die(json_encode(["error" => "Toate campurile sunt necesare."]));
    }

    $data = date("Y-m-d", strtotime($_POST["data"]));

    $mysqli = require __DIR__ . "/database.php";

    $sql = "INSERT INTO tranzactii (tip, categorie, descriere, suma, data) VALUES (?, ?, ?, ?, ?)";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('sssds', $_POST["tip"], $_POST["categorie"], $_POST["descriere"], $_POST["suma"], $data);

    if ($stmt->execute()) {
        echo json_encode(["message" => "Tranzactie reusita!", "data" => $_POST]);
    } else {
        die(json_encode(["error" => "Error: " . $stmt->error]));
    }

    $stmt->close();
    $mysqli->close();
}
?>
