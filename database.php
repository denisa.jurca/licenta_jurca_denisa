<?php
/*
Scopul acestui fișier PHP este de a servi ca fișier de configurare care facilitează conectarea la o bază de date MySQL. 
Mai jos este o prezentare concisă a funcționalității sale:

Sunt configurate setările pentru gazdă (localhost), numele bazei de date (aplicație), numele de utilizator (root) și 
parola (aplicație). Folosind obiectul mysqli, o conexiune este stabilită și ulterior returnată pentru utilizare în alte secțiuni ale 
codului.
În cazul unui eșec în stabilirea conexiunii la baza de date, va fi prezentată o notificare de eroare. Această bucată de cod permite 
aplicației să stabilească comunicarea cu baza de date MySQL, permițând executarea sarcinilor esențiale de citire și scriere.
*/

$host="localhost";
$dbname="aplicatie";
$username="root";
$password="aplicatie";

$mysqli = new mysqli(hostname: $host, username: $username, password: $password, database: $dbname, port: 3307);

if($mysqli->connect_errno){
    die("Connection error:" . $mysqli->connect_error);
}

return $mysqli;
?>