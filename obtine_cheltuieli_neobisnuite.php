<?php
/*Scopul acestui fișier PHP este să gestioneze și să analizeze datele care sunt trimise printr-un formular HTML și, ulterior, 
să furnizeze rezultatele în format JSON. Mai jos este o prezentare concisă a funcționalității sale:

Datele primite sunt verificate pentru a valida prezența unei valori numerice valide pentru suma de cheltuială, iar ulterior 
recuperează această valoare.

Pentru a obtine cheltuielile lunare din anul precedent care depășesc o sumă specificată, se execută o interogare a bazei de date. 
Pentru a proteja împotriva injecțiilor SQL, este folosită o interogare parametrizată.

Rezultatele vor fi în formatul unui tabel asociativ. De asemenea, se identifica și se afiseaza cheltuielile maxime pentru fiecare lună.

Rezultatele sunt returnate în format JSON, permițând codificarea datelor și prelucrarea ulterioară în JavaScript de către 
client.

În contextul gestionării erorilor, este se abordeze scenariile în care nu se primesc date sau când suma primită este 
considerată nevalidă și, ulterior, se prezinte mesaje de eroare adecvate contextului.
*/
include_once('database.php');

if (isset($_POST['suma']) && is_numeric($_POST['suma'])) {

    $suma = $_POST['suma'];

    $anul_trecut = date('Y') - 1;

    $nume_luni_romana = array(
        1 => 'Ianuarie',
        2 => 'Februarie',
        3 => 'Martie',
        4 => 'Aprilie',
        5 => 'Mai',
        6 => 'Iunie',
        7 => 'Iulie',
        8 => 'August',
        9 => 'Septembrie',
        10 => 'Octombrie',
        11 => 'Noiembrie',
        12 => 'Decembrie'
    );

    $sql = "SELECT MONTH(data) as luna, SUM(suma) as total_cheltuieli
            FROM tranzactii 
            WHERE tip = 'cheltuiala' AND YEAR(data) = ? AND data >= MAKEDATE(? - 1, 1) AND data < MAKEDATE(? + 1, 1)
            GROUP BY MONTH(data)
            HAVING total_cheltuieli > ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('iiid', $anul_trecut, $anul_trecut, $anul_trecut, $suma);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $output = [];
        while ($row = $result->fetch_assoc()) {
            $luna_numerica = $row['luna'];
            $total_cheltuieli = $row['total_cheltuieli'];
            $luna_romana = $nume_luni_romana[$luna_numerica];

            $sql_max_cheltuiala = "SELECT categorie, descriere, suma 
                                   FROM tranzactii 
                                   WHERE tip = 'cheltuiala' AND YEAR(data) = ? AND MONTH(data) = ? 
                                   ORDER BY suma DESC 
                                   LIMIT 1";
            $stmt_max_cheltuiala = $mysqli->prepare($sql_max_cheltuiala);
            $stmt_max_cheltuiala->bind_param('ii', $anul_trecut, $luna_numerica);
            $stmt_max_cheltuiala->execute();
            $result_max_cheltuiala = $stmt_max_cheltuiala->get_result();

            if ($result_max_cheltuiala->num_rows > 0) {
                $row_max_cheltuiala = $result_max_cheltuiala->fetch_assoc();
                $categorie_cheltuiala_maxima = $row_max_cheltuiala['categorie'];
                $descriere_cheltuiala_maxima = $row_max_cheltuiala['descriere'];
                $suma_cheltuiala_maxima = $row_max_cheltuiala['suma'];

                $output[] = [
                    'luna' => $luna_romana,
                    'total_cheltuieli' => $total_cheltuieli,
                    'categorie_maxima' => $categorie_cheltuiala_maxima,
                    'descriere_maxima' => $descriere_cheltuiala_maxima,
                    'suma_maxima' => $suma_cheltuiala_maxima
                ];
            }
        }
    
        echo json_encode($output);
    } else {
        echo json_encode(['error' => 'Nu există lunile în care suma cheltuielilor depășește limita introdusă.']);
    }

    $stmt->close();
} else {
    
    echo json_encode(['error' => 'Eroare: Nu s-a primit limita sau limita este invalidă.']);
}

$mysqli->close();
?>
