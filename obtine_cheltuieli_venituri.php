
<?php
/*
Următorul cod PHP este conceput pentru a prelua datele trimise printr-un formular, selectând în mod specific un an, și, ulterior,
să afișeze veniturile și cheltuielile lunare pentru anul ales. O prezentare concisă a codului este oferită mai jos:

În procesul de selecție a anului, este necesar să se verifice prezența unei valori postate pentru parametrul de selecție a anului 
(selecteazaAn). În cazul în care o astfel de valoare este prezentă, aceasta va fi utilizată în mod corespunzător. În schimb, dacă nu 
se înregistrează nicio valoare, anul curent va fi folosit ca opțiune implicită.

Interogare SQL: Construiește o interogare de bază de date care calculează totalul atât al veniturilor, cât și al cheltuielilor 
pentru anul desemnat. Se aplică funcția YEAR() pentru a restricționa tranzacțiile în funcție de anul ales.

Executarea interogării duce la preluarea rezultatelor, care sunt ulterior stocate într-o matrice asociativă.

În timpul procesului de extragere a datelor, este necesar să se examineze cu meticulozitate rândurile rezultatelor obținute pentru a 
identifica și extrage informațiile pertinente, inclusiv etichetele lunii, sumele veniturilor și sumele cheltuielilor, toate acestea 
fiind stocate în matrice distincte.

La preluarea datelor necesare, se inchide conexiunea la baza de date pentru a elibera resurse valoroase.*/

include_once('database.php');

$selectedYear = isset($_POST['selecteazaAn']) ? $_POST['selecteazaAn'] : date('Y');

$sql = "SELECT data, SUM(IF(tip='venit' AND YEAR(data) = $selectedYear, suma, 0)) AS venituri, SUM(IF(tip='cheltuiala' 
AND YEAR(data) = $selectedYear, suma, 0)) AS cheltuieli FROM tranzactii GROUP BY MONTH(data)";
$result = $mysqli->query($sql);

$labels = [];
$venituri = [];
$cheltuieli = [];

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $labels[] = date('F', strtotime($row['data'])); 
        $venituri[] = $row['venituri'];
        $cheltuieli[] = $row['cheltuieli'];
    }
}

$mysqli->close();
?>
