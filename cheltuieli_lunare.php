<?php 
/*Următorul cod PHP și JavaScript exemplifica o pagină web concepută pentru a facilita monitorizarea și administrarea cheltuielilor 
lunare. Mai jos este o prezentare concisă a caracteristicilor sale:

Interfața de utilizator a paginii include un titlu și un meniu, oferind utilizatorului opțiunea de a alege luna dorită pentru 
vizualizarea cheltuielilor. 

Utilizând biblioteca Chart.js, este generată o diagramă cu bare pentru a reprezenta vizual cheltuielile pentru luna selectată. 
Cheltuielile sunt clasificate și afișate pe axa orizontală, în timp ce sumele corespunzătoare sunt prezentate pe axa verticală.

Comunicarea cu serverul are loc atunci când utilizatorul alege o anumită lună, declanșând o solicitare de recuperare a cheltuielilor 
lunare corespunzătoare. Ulterior, răspunsul JSON este analizat și utilizat pentru a modifica valorile afișate în diagramă.

În contextul formatării numerelor, funcția numar_formatat() are scopul de a îmbunătăți reprezentarea vizuală a numerelor prin 
încorporarea separatorilor de mii și permițând personalizarea zecimalelor.*/

include_once('sesiune.php');
include_once('meniu.php'); 
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cheltuieli pe lună și Diagramă</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="aplicatie_design.css">
</head>
<body>

<header>
    <img src="logo.png" alt="Logo" class="logo-parte-sus">
</header>

<div class="container mt-5">
        <div class="row justify-content-center">
            <div class="titlu-cheltuieli-lunare">
                <h2 class="text-center">
                  <img src="chelt-lun.png" alt="Cheltuieli_Lunare" class="bani-pictograma"> CHELTUIELI LUNARE: <span id="cheltuieli_lunare"></span>
                </h2>
            </div>
        </div>

        <div id="selectare_luna_container">
            <div id="selectare_luna">
                <form method="post">
                    <div class="mb-3">
                        <label for="selecteazaLuna" class="form-label">Selectează luna </label>
                        <select id="selecteazaLuna" name="selecteazaLuna" class="form-select">
                            <option value="1">Ianuarie</option>
                            <option value="2">Februarie</option>
                            <option value="3">Martie</option>
                            <option value="4">Aprilie</option>
                            <option value="5">Mai</option>
                            <option value="6">Iunie</option>
                            <option value="7">Iulie</option>
                            <option value="8">August</option>
                            <option value="9">Septembrie</option>
                            <option value="10">Octombrie</option>
                            <option value="11">Noiembrie</option>
                            <option value="12">Decembrie</option>
                        </select>
                    </div>
                </form>
            </div>

            <div id="diagrama_luni_container">
                <canvas id="diagramaLuni"></canvas>
            </div>
        </div>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script>

        document.getElementById("selecteazaLuna").addEventListener("change", function (event) {
                    var lunaSelectata = this.value;

                    fetch('obtine_cheltuieli_lunare.php?month=' + lunaSelectata)
                        .then(response => response.json())
                        .then(data => {
                            document.getElementById('cheltuieli_lunare').innerText = numar_formatat(data.totalCheltuieliLunare, 2);
                            desenareDiagCheltLunare(data.categorii, data.sume);
                        })
                        .catch(error => {
                            console.error('Error:', error);
                        });
                });

        document.addEventListener("DOMContentLoaded", function () {
            fetch('obtine_cheltuieli_lunare.php?month=1')
                .then(response => response.json())
                .then(data => {
                    document.getElementById('cheltuieli_lunare').innerText = numar_formatat(data.totalCheltuieliLunare, 2);
                    desenareDiagCheltLunare(data.categorii, data.sume);
                })
                .catch(error => {
                    console.error('Error:', error);
                });
        });

        var ctx = document.getElementById('diagramaLuni').getContext('2d');
        var chart;

        function desenareDiagCheltLunare(categorii, sume) {
            if (chart) {
                chart.destroy();
            }

            chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: categorii,
                    datasets: [{
                        label: 'Cheltuieli pe Lună',
                        data: sume,
                        backgroundColor: 'rgba(255, 0, 0, 0.8)',
                        borderColor: 'rgba(255, 0, 0, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        }

        function numar_formatat(numar, zecimale, separatorZecimal, separatorMii) {
            numar = parseFloat(numar);
            if (!zecimale) zecimale = 0;
            if (!separatorZecimal) separatorZecimal = '.';
            if (!separatorMii) separatorMii = ',';

            var numarRotunjit = Math.pow(10, zecimale);
            numar = Math.round(numar * numarRotunjit) / numarRotunjit;

            var parti = numar.toString().split(".");
            parti[0] = parti[0].replace(/\B(?=(\d{3})+(?!\d))/g, separatorMii);

            return parti.join(separatorZecimal);
        }

    </script>
</body>
</html>

