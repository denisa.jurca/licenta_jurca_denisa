<?php 
/*
Codul PHP și HTML furnizat cuprinde o pagină web care prezintă și compară veniturile și cheltuielile pe o perioadă de timp.
Mai jos este prezentată o prezentare concisă a funcționalității pe care le oferă:

Interfața cu utilizatorul a paginii este compusă dintr-un titlu și un formular, oferind utilizatorilor posibilitatea de a introduce anul
dorit în scopuri de vizualizare a datelor.

Pagina utilizează biblioteca Chart.js pentru a prezenta o diagramă cu linii care compară veniturile și cheltuielile lunare pentru anul 
ales. Axa verticală reprezintă veniturile și cheltuielile, în timp ce axa orizontală reprezintă lunile.

Valorile veniturilor și cheltuielilor, sunt obtinute dintr-un fișier PHP care prelucrează 
eficient informațiile și le prezintă sub forma unei matrice JSON.*/

include_once('sesiune.php');
include_once('meniu.php'); 
?>

<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comparare Venituri și Cheltuieli</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link rel="stylesheet" type="text/css" href="aplicatie_design.css">
</head>
<body>

        <header>
            <img src="logo.png" alt="Logo" class="logo-parte-sus">
        </header>

        <div class="container mt-5">
                <div class="row justify-content-center">
                    <div class="titlu-venituri-cheltuieli">
                        <h2 class="text-center">
                        <img src="bani-timp.png" alt="Venituri_Cheltuieli" class="bani-pictograma"> VENITURILE ȘI CHELTUIELILE ÎN TIMP<span id="venituri_cheltuieli"></span>
                        </h2>
                    </div>
                </div>

                <div class="selectareAn-container">
                    <div class="grup-selectie">
                        <form method="POST" class="venituri-cheltuieli-form text-center mt-3">
                                <label for="selecteazaAn">Introduceți anul </label>
                                <input type="number" name="selecteazaAn" id="selecteazaAn" min="yyyy" placeholder="Anul" value="<?php echo isset($_POST['selecteazaAn']) ? $_POST['selecteazaAn'] : date('Y'); ?>" required>
                                <button type="submit" class="butonAn">Afișează</button>
                        </form>
                    </div>
        
                    <div class="diagrama-comp-container mt-3">
                        <canvas id="diagramaComparatie"></canvas>
                    </div>
                </div>
        </div>

    <?php include_once('obtine_cheltuieli_venituri.php'); ?>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script>

        var labels = <?php echo json_encode($labels); ?>;
        var venituri = <?php echo json_encode($venituri); ?>;
        var cheltuieli = <?php echo json_encode($cheltuieli); ?>;

        var ctx = document.getElementById('diagramaComparatie').getContext('2d');
        var diagramaComparativa = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Venituri',
                    data: venituri,
                    borderColor: 'rgba(0, 149, 199, 1)',
                    backgroundColor: 'rgba(0, 141, 188, 1)',
                    fill: false
                }, {
                    label: 'Cheltuieli',
                    data: cheltuieli,
                    borderColor: 'rgba(255, 20, 147, 0.8)',
                    backgroundColor: 'rgba(212, 66, 149, 0.9)',
                    fill: false
                }]
            },
            options: {
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: 'Lună'
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: 'Sumă'
                        }
                    }
                }
            }
        });
    </script>
</body>
</html>
